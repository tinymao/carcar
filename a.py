import RPi.GPIO as gpio
import time
import readchar


def init():
    gpio.setmode(gpio.BOARD)
    gpio.setup(7, gpio.OUT, initial=gpio.HIGH)
    gpio.setup(11, gpio.OUT, initial=gpio.HIGH)
    gpio.setup(13, gpio.OUT, initial=gpio.HIGH)
    gpio.setup(15, gpio.OUT, initial=gpio.HIGH)

def forward(tf):
    gpio.output(7, False)
    gpio.output(11, True)
    gpio.output(13, True)
    gpio.output(15, False)
    time.sleep(tf)
    stop()

def backward(tf):
    gpio.output(7, True)
    gpio.output(11, False)
    gpio.output(13, False)
    gpio.output(15, True)
    time.sleep(tf)
    stop()

def turnLeft(tf):
    gpio.output(7, True)
    gpio.output(11, True)
    gpio.output(13, True)
    gpio.output(15, False)
    time.sleep(tf)
    stop()

def turnRight(tf):
    gpio.output(7, False)
    gpio.output(11, True)
    gpio.output(13, False)
    gpio.output(15, False)
    time.sleep(tf)
    stop()

def pivot_left(tf):
    gpio.output(7, True)
    gpio.output(11, False)
    gpio.output(13, True)
    gpio.output(15, False)
    time.sleep(tf)
    stop()

def pivot_right(tf):
    gpio.output(7, False)
    gpio.output(11, True)
    gpio.output(13, False)
    gpio.output(15, True)
    time.sleep(tf)
    stop()

def stop():
    gpio.output(7, False)
    gpio.output(11, False)
    gpio.output(13, False)
    gpio.output(15, False)


init()
if __name__ == "__main__":

    print "Press 'g' to quit..."

    while True:

        ch = readchar.readkey()
        sleep_time = 0.5

        if ch == 'w':
            forward(sleep_time)

        elif ch == 's':
            backward(sleep_time)

        elif ch == 'd':
            turnRight(sleep_time)

        elif ch == 'a':
            turnLeft(sleep_time)

        elif ch == 'q':
            pivot_left(sleep_time)

        elif ch == 'e':
            pivot_right(sleep_time)

        elif ch == 'g':
            print "\nQuit"
            gpio.cleanup()
            quit()