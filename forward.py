import RPi.GPIO as GPIO
import time
import readchar


GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
pwm7 = GPIO.PWM(7, 200)
pwm11 = GPIO.PWM(11, 200)
pwm13 = GPIO.PWM(13, 200)
pwm15 = GPIO.PWM(15, 200)
pwm7.start(0)
pwm11.start(0)
pwm13.start(0)
pwm15.start(0)


def forward(tf, duty):
    pwm7.ChangeDutyCycle(0)
    pwm11.ChangeDutyCycle(duty)
    pwm13.ChangeDutyCycle(duty)
    pwm15.ChangeDutyCycle(0)
    time.sleep(tf)
    stop()

def backward(tf, duty):
    pwm7.ChangeDutyCycle(duty)
    pwm11.ChangeDutyCycle(0)
    pwm13.ChangeDutyCycle(0)
    pwm15.ChangeDutyCycle(duty)

    time.sleep(tf)
    stop()

def turnLeft(tf, duty):
    pwm7.ChangeDutyCycle(duty)
    pwm11.ChangeDutyCycle(duty)
    pwm13.ChangeDutyCycle(duty)
    pwm15.ChangeDutyCycle(0)
    time.sleep(tf)
    stop()

def turnRight(tf, duty):
    pwm7.ChangeDutyCycle(0)
    pwm11.ChangeDutyCycle(duty)
    pwm13.ChangeDutyCycle(0)
    pwm15.ChangeDutyCycle(0)
    time.sleep(tf)
    stop()

def pivot_left(tf, duty):
    pwm7.ChangeDutyCycle(duty)
    pwm11.ChangeDutyCycle(0)
    pwm13.ChangeDutyCycle(duty)
    pwm15.ChangeDutyCycle(0)
    time.sleep(tf)
    stop()

def pivot_right(tf, duty):
    pwm7.ChangeDutyCycle(0)
    pwm11.ChangeDutyCycle(duty)
    pwm13.ChangeDutyCycle(0)
    pwm15.ChangeDutyCycle(duty)
    time.sleep(tf)
    stop()

def stop():
    pwm7.ChangeDutyCycle(0)
    pwm11.ChangeDutyCycle(0)
    pwm13.ChangeDutyCycle(0)
    pwm15.ChangeDutyCycle(0)


if __name__ == "__main__":

    print "Press 'g' to quit..."

    while True:

        ch = readchar.readkey()
        duty = 30
        sleep_time = 0.5

        if ch == 'w':
            forward(sleep_time, duty)

        elif ch == 's':
            backward(sleep_time, duty)

        elif ch == 'd':
            turnRight(sleep_time, duty)

        elif ch == 'a':
            turnLeft(sleep_time, duty)

        elif ch == 'q':
            pivot_left(sleep_time, duty)

        elif ch == 'e':
            pivot_right(sleep_time, duty)

        elif ch == 't':
            if duty <= 100:
                duty = duty + 1
            else:
                duty = 100

        elif ch == 'g':
            print "\nQuit"
            GPIO.cleanup()
            quit()